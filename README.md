# rate-my-air

A NodeMCU air quality monitor built around the Bosch BME680 and a u8g compatible 128x64 OLED display

## Air Quality Index

The BME680 provides a value for gas resistance not AQI. Sadly Bosch has chosen to place the logic to convert gas resistance into AQI in a closed source library ([BSEC](https://www.bosch-sensortec.com/software-tools/software/bsec/)). Luckily [Pimoroni](https://shop.pimoroni.com/) have provided example code in their python library https://github.com/pimoroni/bme680-python/blob/master/examples/indoor-air-quality.py. This might not be as good as the Bosch logic but without access to the source there is no way to tell.

I have chosen to implement the Pimoroni version rather than try to get BSEC running on the NodeMCU

## Libraries

- u8g2
- bme680
