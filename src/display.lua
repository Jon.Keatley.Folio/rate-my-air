
function newDisplay(bus_id)
  local nd = {}
  local sla = 0x3C
  nd.disp = u8g2.ssd1306_i2c_128x64_noname(bus_id, sla)

  nd.width = 126
  nd.height = 64


  nd.draw = function(tmp, humidity, gas, index)
    nd.disp:clearBuffer()
    nd.disp:setFont(u8g2.font_6x10_tf)
    nd.disp:drawStr(1,11,"Tmp:" .. tmp)
    nd.disp:drawStr(64,11,"Hum:" .. humidity)


    nd.disp:setFont(u8g2.font_10x20_tf)
    nd.disp:drawStr(1,35,"Gas:" .. gas)
    nd.disp:drawStr(1,50,"IAQ:" .. index)
    nd.disp:sendBuffer()

  end

  return nd
end
