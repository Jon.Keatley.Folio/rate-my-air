
require "sensor"
require "display"

function update()

  sensor:read()
  print(sensor.burn_in_time)
  display.draw(sensor.tmp, sensor.humidity, sensor.gas, sensor.air_quality_score)
  updateTmr:start()
end

-- SDA == blue
-- SCL == red
-- GND == grey
-- VCC == green

function main()
  -- setup i2c interface
  print("Setting up i2c")
  local bus_id  = 0
  local sda = 3 -- GPIO0
  local scl = 4 -- GPIO2
  i2c.setup(bus_id, sda, scl, i2c.SLOW)

  local alt=22 -- altitude of the measurement place https://routecalculator.co.uk/elevation

  print("Setting up display")
  -- setup display
  display = newDisplay(bus_id)
  display.draw("-", "-", "-","-")

  print("Setting up BME680")
  -- setup bme680
  sensor = newSensor(alt, 2000)


  -- start clock for updates
  updateTmr = tmr.create()
  updateTmr:register(2000,tmr.ALARM_SEMI, function() -- semi auto to protect against over run
    update()
  end)
  updateTmr:start()
end

-- delayed init to protect against errors flash blocking
local init = tmr.create()
init:register(1000,tmr.ALARM_SINGLE, function ()
  main()
	init:unregister()
end)
init:start()
