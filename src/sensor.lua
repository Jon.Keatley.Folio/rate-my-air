
-- bme680.read()
-- T temperature in Celsius as an integer multiplied with 100 - yes
-- P air pressure in hectopascals multiplied by 100
-- H relative humidity in percent multiplied by 1000 - yes
-- G gas resistance
-- QNH air pressure in hectopascals multiplied by 100 converted to sea level
-- https://en.wikipedia.org/wiki/Air_quality_index#United_Kingdom
-- AQI - 1 - 10
-- 1 -3

--sensor.set_humidity_oversample(bme680.OS_2X)
--sensor.set_pressure_oversample(bme680.OS_4X)
--sensor.set_temperature_oversample(bme680.OS_8X)
--sensor.set_filter(bme680.FILTER_SIZE_3)
--sensor.set_gas_status(bme680.ENABLE_GAS_MEAS)

--sensor.set_gas_heater_temperature(320)
--sensor.set_gas_heater_duration(150)
--sensor.select_gas_heater_profile(0)

function sum(t)
    local sum = 0
    for k,v in pairs(t) do
        sum = sum + v
    end

    return sum
end


function newSensor(alt, sleep_time)
  local s = {}

  bme680.setup()
  s.alt = alt
  s.tmp = "-"
  s.humidity = "-"
  s.gas = "-"
  s.air_quality_score = "???"
  s.sleep_time = sleep_time
  s.burn_in_time =5 * 60 * 1000
  s.heater_duration = 150

  s.gas_record_count = 50
  s.gas_records = {}
  for i = 1,50 do
    table.insert(s.gas_records,0)
  end
  s.gas_baseline = 0

  s.read = function()
    bme680.startreadout(s.heater_duration, function()
      T, P, H, G, QNH = bme680.read(alt)
      if T then
        local Tsgn = (T < 0 and -1 or 1); T = Tsgn*T
        --print(string.format("T=%s%d.%02d", Tsgn<0 and "-" or "", T/100, T%100))
        s.tmp = string.format("%s%d.%02d", Tsgn<0 and "-" or "", T/100, T%100)
        s.humidity = string.format("%d.%02d%%", H/1000, H%1000)
        s.gas = string.format("%d", G)

        -- update readings chart
        table.insert(s.gas_records,G)
        table.remove(s.gas_records, 1)

        -- need to build 300 seconds (5 minutes) of readings
        if s.burn_in_time > 0 then
          s.burn_in_time = s.burn_in_time - s.sleep_time
          s.burn_in_time = s.burn_in_time - s.heater_duration
        else
          if s.gas_baseline == 0 then
            s.gas_baseline = sum(s.gas_records) / 50
            print("Base line" .. s.gas_baseline)
          end
          s.calculateAQ(G, H/1000 + H%1000)
        end

        --print(string.format("QFE=%d.%03d", P/100, P%100))
        --print(string.format("QNH=%d.%03d", QNH/100, QNH%100))
        --print(string.format("humidity=%d.%03d%%", H/1000, H%1000))
        --print(string.format("gas resistance=%d", G))
        --D = bme680.dewpoint(H, T)
        --local Dsgn = (D < 0 and -1 or 1); D = Dsgn*D
        --print(string.format("dew_point=%s%d.%02d", Dsgn<0 and "-" or "", D/100, D%100))
      end
    end)
  end

  s.calculateAQ = function(gas, humidity)
    local hum_score = 0
    local gas_score = 0
    local hum_weighting = 0.25
    local hum_baseline = 40
    local gas_offset = s.gas_baseline - gas
    local hum_offset = humidity - hum_baseline

    --Calculate hum_score as the distance from the hum_baseline.
    if hum_offset > 0 then
        hum_score = (100 - hum_baseline - hum_offset)
        hum_score = hum_score / (100 - hum_baseline)
        hum_score = hum_score * (hum_weighting * 100)

    else
        hum_score = (hum_baseline + hum_offset)
        hum_score = hum_score / hum_baseline
        hum_score = hum_score * (hum_weighting * 100)
    end

    --Calculate gas_score as the distance from the gas_baseline.
    if gas_offset > 0 then
        gas_score = (gas / s.gas_baseline)
        gas_score = gas_score * (100 - (hum_weighting * 100))

    else
        gas_score = 100 - (hum_weighting * 100)
    end

    --Calculate air_quality_score.
    s.air_quality_score = string.format("%d", hum_score + gas_score)

  end

  return s
end
